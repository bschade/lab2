package edu.ucsd.cs110w.temperature;
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return Float.toString(getValue()) + " C";
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		float newValue = getValue();
		newValue *= 9;
		newValue /= 5;
		newValue += 32;
		return new Fahrenheit(newValue);
	}
	@Override
	public Temperature toKelvin() {
		float newValue = getValue();
		newValue += 273;
		return new Kelvin(newValue);
	}
}
