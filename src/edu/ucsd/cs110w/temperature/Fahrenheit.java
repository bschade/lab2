package edu.ucsd.cs110w.temperature;
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return Float.toString(getValue()) + " F";
	}
	@Override
	public Temperature toCelsius() {
		float newValue = getValue();
		newValue -= 32;
		newValue *= 5;
		newValue /= 9;
		return new Celsius(newValue);
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	@Override
	public Temperature toKelvin() {
		float newValue = getValue();
		newValue -= 32;
		newValue *= 5;
		newValue /= 9;
		newValue += 273;
		return new Kelvin(newValue);
	}
}
