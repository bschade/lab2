package edu.ucsd.cs110w.temperature;

public class Kelvin extends Temperature
{
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		return Float.toString(getValue()) + " K";
	}
	@Override
	public Temperature toCelsius() {
		float newValue = getValue();
		newValue -= 273;
		return new Celsius(newValue);
	}
	@Override
	public Temperature toFahrenheit() {
		float newValue = getValue();
		newValue -= 273;
		newValue *= 9;
		newValue /= 5;
		newValue += 32;
		return new Fahrenheit(newValue);
	}
	@Override
	public Temperature toKelvin() {
		return this;
	}
	
}
